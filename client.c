// Write CPP code here
#include <netdb.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include "utils.h"

#define MAX 80
#define PORT 8888
#define SA struct sockaddr

void processs_program(int sockfd, fd_set *readfds) {
  char buff[1025], valread;
  int n;
  for (;;) {
    FD_ZERO(readfds);
    //add master socket to set
    FD_SET(0, readfds);
    FD_SET(sockfd, readfds);
    select(sockfd + 1, readfds, NULL, NULL, NULL);

    if (FD_ISSET(sockfd, readfds)) {
      if ((valread = read(sockfd, buff, 1024)) != 0) {
        buff[valread] = '\0';
        colorprint("from Server : %s", KCYN, buff);

      }
    }

    if (FD_ISSET(0, readfds)) {
      if ((valread = read(0, buff, 1024)) != 0) {
        buff[valread] = '\0';
        send(sockfd, buff, strlen(buff), 0);
      }
    }
  }
}

int initiate_socket() {
  int sockfd;
  // socket create and varification
  sockfd = socket(AF_INET, SOCK_STREAM, 0);
  if (sockfd == -1) {
    sysprint("socket creation failed...\n");
    exit(0);
  } else
    sysprint("Socket successfully created..\n");

  return sockfd;
}

int connect_socket(int sockfd, fd_set *readfds) {
  struct sockaddr_in servaddr;
  int a;
  bzero(&servaddr, sizeof(servaddr));

  // assign IP, PORT
  servaddr.sin_family = AF_INET;
  servaddr.sin_addr.s_addr = inet_addr("127.0.0.1");
  servaddr.sin_port = htons(PORT);

  // connect the client socket to server socket
  if ((a = connect(sockfd, (SA *) &servaddr, sizeof(servaddr))) != 0) {
    sysprint("connection with the server failed...\n");
    exit(0);
  } else
    sysprint("connected to the server..%d\n", a);
}

int main() {
  fd_set readfds;
  int sockfd = initiate_socket(readfds);
  connect_socket(sockfd, &readfds);
  processs_program(sockfd, &readfds);
  close(sockfd);
}
