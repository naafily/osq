//Example code: A simple server side code, which echos back the received message.
//Handle multiple socket connections with select and fd_set on Linux
#include <stdio.h>
#include <time.h>
#include <string.h> //strlen
#include <stdlib.h>
#include <errno.h>
#include <unistd.h> //close
#include <arpa/inet.h> //close
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <sys/time.h> //FD_SET, FD_ISSET, FD_ZERO macros
#include "utils.h"

#define TRUE 1
#define FALSE 0
#define PORT 8888
#define MAX_USERS_SIZE 30
#define QUESTIONS_INTERVAL 10

int seconds_to_start;
int start_hour, start_min;
struct tm *timeinfo;
int in_broadcast_time = 0;
int current_question = -1;
int tokens_count = 0;
time_t last_question_broadcast = 0;


typedef struct Question_struct {
    char question[100];
    char correct_ans;
} Question;


typedef struct user_struct {
    char username[30];
    char password[30];
    char email[45];
    int diamonds;
    int sd;
    int current_question;
    int will_be_on_quiz;
    int money;
    time_t last_ads;
} User;


Question *create_questions() {
  Question *qustions = (Question *) malloc(5 * sizeof(Question));
  qustions[0] = (Question) {.question="5 + 3 = ?\n1) 8\t2) 9\t3) 10\n", .correct_ans='1'};
  qustions[1] = (Question) {.question= "3 + 4 = ?\n1) 2\t2) 7\t3) 6\n", .correct_ans = '2'};
  qustions[2] = (Question) {.question= "6 + 4 = ?\n1) 11\t2) 13\t3) 10\n", .correct_ans = '3'};
  qustions[3] = (Question) {.question= "2 * 3 = ?\n1) 66\t2) 5\t3) 6\n", .correct_ans = '3'};
  qustions[4] = (Question) {.question= "1 - 4 = ?\n1) -3\t2) 3\t3) 4\n", .correct_ans = '1'};

  return qustions;
}

void login(int sd, char *tokeniozed_request[], User *users) {
  if (get_user_by_sd(sd, users) != -1) {
    send(sd, "FAILED\n", strlen("FAILED\n"), 0);
    return;
  }
  for (int i = 0; i < MAX_USERS_SIZE; ++i)
    if (strncmp(users[i].username, tokeniozed_request[1], 30) == 0)
      if (strncmp(users[i].password, tokeniozed_request[2], 30) == 0) {
        if (users[i].sd != -1)
          send(users[i].sd, "ACTIVE SESSION TERMINATED\n", strlen("ACTIVE SESSION TERMINATED\n"), 0);
        users[i].sd = sd;
        send(sd, "SUCCESS\n", strlen("SUCCESS\n"), 0);
        return;
      }
  send(sd, "FAILED\n", strlen("FAILED\n"), 0);
}

int get_user_by_username(char username[], User *users) {
  for (int i = 0; i < MAX_USERS_SIZE; ++i)
    if (strncmp(users[i].username, username, 30) == 0)
      return i;
  return -1;
}

int get_user_by_sd(int sd, User *users) {
  for (int i = 0; i < MAX_USERS_SIZE; ++i) {
    if (users[i].sd == sd)
      return i;
  }
  return -1;
}

int get_user_by_email(char *email, User *users) {
  for (int i = 0; i < MAX_USERS_SIZE; ++i)
    if (strncmp(users[i].email, email, 45) == 0)
      return i;
  return -1;
}


void signup(int sd, char *tokenized_request[], User *users) {
  if (!update_will_be_needed(timeinfo))
    return;
  if (in_broadcast_time)
    return;
  if (get_user_by_username(tokenized_request[1], users) != -1) {
    send(sd, "FAILED - DUPLICATE\n", strlen("FAILED - DUPLICATE\n"), 0);
    return;
  }

  for (int i = 0; i < MAX_USERS_SIZE; ++i) {
    if (!strlen(users[i].username)) {
      strncpy(users[i].username, tokenized_request[1], sizeof(users[i].username) - 1);
      strncpy(users[i].password, tokenized_request[2], sizeof(users[i].password) - 1);
      users[i].sd = sd;
      logger(LOG_INFO_LEVEL, "Created User: %d\n", sd);
      send(sd, "SUCCESS\n", strlen("SUCCESS\n"), 0);
      return;
    }
  }
  send(sd, "FAILED - FULL SERVER\n", strlen("FAILED - FULL SERVER\n"), 0);
}


void logout(int sd, User *users) {
  for (int i = 0; i < MAX_USERS_SIZE; ++i)
    if (users[i].sd == sd)
      users[i].sd = -1;
  send(sd, "SUCCESS\n", strlen("SUCCESS\n"), 0);
}

void email(int sd, char *tokenized_request[], User *users) {
  for (int i = 0; i < MAX_USERS_SIZE; ++i) {
    if (users[i].sd == sd) {
      if (!strlen(users[i].email)) {
        users[i].diamonds = users[i].diamonds + 1;
        strncpy(users[i].email, tokenized_request[1], sizeof(users[i].email) - 1);
        send(sd, "SUCCESS\n", strlen("SUCCESS\n"), 0);
        return;
      }
    }
  }
  send(sd, "FAILED - EMAIL SET\n", strlen("FAILED - EMAIL SET\n"), 0);
}

void diamond(int sd, User *users) {
  char diamond_count[5];
  for (int i = 0; i < MAX_USERS_SIZE; ++i)
    if (users[i].sd == sd) {
      sprintf(diamond_count, "%d\n", users[i].diamonds);
      send(sd, diamond_count, strlen(diamond_count), 0);
      return;
    }
  send(sd, "FAILED - AUTHENTICATION\n", strlen("FAILED - AUTHENTICATION\n"), 0);
}


void money(int sd, User *users) {
  char money[10];
  for (int i = 0; i < MAX_USERS_SIZE; ++i)
    if (users[i].sd == sd) {
      sprintf(money, "%d\n", users[i].money);
      send(sd, money, strlen(money), 0);
      return;
    }
  send(sd, "FAILED - AUTHENTICATION\n", strlen("FAILED - AUTHENTICATION\n"), 0);
}

void submit_ans(int sd, char *splitted_token[], User *users, Question *questions) {
  int user_id = get_user_by_sd(sd, users);
  if (user_id == -1) {
    send(sd, "FAILED - AUTHENTICATION\n", strlen("FAILED - AUTHENTICATION\n"), 0);
    return;
  }
  if (!users[user_id].will_be_on_quiz) {
    send(sd, "FAILED - FORBIDDEN\n", strlen("FAILED - FORBIDDEN\n"), 0);
    return;
  }
  if (current_question != users[user_id].current_question) {
    logger(LOG_INFO_LEVEL, "%d %d\n", current_question, users[user_id].current_question);
    send(sd, "CONFLICT\n", strlen("CONFLICT\n"), 0);
    return;
  } else {
    users[user_id].current_question++;
  }

  if (questions[current_question].correct_ans == splitted_token[0][0])
    send(sd, "CORRECT\n", strlen("CORRECT!\n"), 0);
  else {
    users[user_id].diamonds--;
    send(sd, "WRONG\n", strlen("WRONG\n"), 0);
  }
}


void chat(int sd, char *splitted_token[], User *users) {
  int user_id = get_user_by_username(splitted_token[1], users);
  if (user_id == -1) {
    send(sd, "FAILED - 404\n", strlen("FAILED\n"), 0);
    return;
  }
  if (users[user_id].sd < 2) {
    send(sd, "FAILED - OFFLINE USER\n", strlen("FAILED - OFFLINE USER\n"), 0);
    return;
  }
  char message[100];
  int k = 0;
  for (int i = 2; i < tokens_count - 1; ++i) {
    for (int j = 0; j < strlen(splitted_token[i]); ++j)
      message[k++] = splitted_token[i][j];
    message[k++] = ' ';
  }
  message[k++] = '\n';
  message[k++] = '\0';
  char header[200] = "\nNew message from: @";
  strcat(header, users[user_id].username);
  strcat(header, ": ");
  strcat(header, message);
  send(users[user_id].sd, header, strlen(header), 0);
}


void process_request(int sd, char request[], User *users, Question *questions) {
  tokens_count = 1;
  char *ptr = strtok(request, " ");
  if (!ptr)
    return;

  char *splitted_token[10];
  splitted_token[0] = ptr;
  int i = 1;
  while (ptr) {
    ptr = strtok(NULL, " ");
    splitted_token[i] = ptr;
    ++i;
    tokens_count++;
  }
  if (in_broadcast_time) {
    submit_ans(sd, splitted_token, users, questions);
  }
  if (strncmp(splitted_token[0], "login", 5) == 0) {
    login(sd, splitted_token, users);
  } else if (strncmp(splitted_token[0], "signup", strlen("signup")) == 0) {
    signup(sd, splitted_token, users);
  } else if (strncmp(splitted_token[0], "logout", strlen("logout")) == 0) {
    logout(sd, users);
  } else if (strncmp(splitted_token[0], "email", strlen("email")) == 0) {
    email(sd, splitted_token, users);
  } else if (strncmp(splitted_token[0], "diamonds", strlen("diamonds")) == 0) {
    diamond(sd, users);
  } else if (strncmp(splitted_token[0], "money", strlen("money")) == 0) {
    money(sd, users);
  } else if (strncmp(splitted_token[0], "chat", strlen("chat")) == 0) {
    chat(sd, splitted_token, users);
  }

}


void broadcast_question(Question *questions, User *users) {
  if (time(NULL) - last_question_broadcast < QUESTIONS_INTERVAL)
    return;
  last_question_broadcast = time(NULL);
  current_question++;
  if (current_question == 5) {
    in_broadcast_time = 0;
    for (int i = 0; i < MAX_USERS_SIZE; ++i)
      if (users[i].sd > 0) {
        send(users[i].sd, "FINISH\n", strlen("FINISH\n"), 0);
        if (users[i].will_be_on_quiz && users[i].diamonds >= 0)
          users[i].money += 1000;
      }

    return;
  }
  for (int i = 0; i < MAX_USERS_SIZE; ++i) {
    if (users[i].sd < 1)
      continue;
    if (!current_question)
      send(users[i].sd, "Game starts!\n", strlen("Game starts!\n"), 0);
    if (users[i].current_question == current_question - 1 && users[i].will_be_on_quiz)
      users[i].diamonds--;
    users[i].current_question = current_question;
    send(users[i].sd, questions[current_question].question, strlen(questions[current_question].question), 0);
  }
}

int update_will_be_needed(struct tm *now) {
  if (start_hour == now->tm_hour + 1 && (start_min + 60 - now->tm_min) < 2)
    return 1;
  if (start_hour == now->tm_hour && start_min - now->tm_min > 2)
    return 1;
  return 0;

}

void set_will_be(User *users) {
  for (int i = 0; i < MAX_USERS_SIZE; ++i) {
    if (users[i].sd > 1)
      users[i].will_be_on_quiz = 1;
    else
      users[i].will_be_on_quiz = 0;
  }
}


void check_ads(User *users) {
  char *ad =
          "\n  ___  ____   ___  \n"
          " / _ \\/ ___| / _ \\ \n"
          "| | | \\___ \\| | | |\n"
          "| |_| |___) | |_| |\n"
          " \\___/|____/ \\__\\_\\\n"
          "                   \n";
  for (int i = 0; i < MAX_USERS_SIZE; ++i) {
    if (users[i].sd > 1) {
      if (time(NULL) - users[i].last_ads > 600) {
        send(users[i].sd, ad, strlen(ad), 0);
        users[i].last_ads = time(NULL);
        users->diamonds++;
      }
    }
  }
}


int main(int argc, char *argv[]) {
  start_hour = atoi(argv[1]);
  start_min = atoi(argv[2]);
  struct timeval tv = {1, 0};
  Question *questions = create_questions();
  User users[MAX_USERS_SIZE];
  int opt = TRUE;
  int master_socket, addrlen, new_socket, client_socket[30],
          max_clients = 30, activity, i, valread, sd;
  int max_sd;
  struct sockaddr_in address;

  char buffer[1025]; //data buffer of 1K

  for (int i = 0; i < MAX_USERS_SIZE; ++i) {
    strncpy(users[i].username, "", sizeof(users[i].username) - 1);
    strncpy(users[i].password, "", sizeof(users[i].username) - 1);
    strncpy(users[i].email, "", sizeof(users[i].email) - 1);
    users[i].sd = -1;
    users[i].diamonds = 5;
    users[i].current_question = 0;
    users[i].will_be_on_quiz = 0;
    users[i].money = 0;
    users[i].last_ads = 0;
  }

  //set of socket descriptors
  fd_set readfds;

  //a message
  char *message = "Daemon Beta Version \r\n";

  //initialise all client_socket[] to 0 so not checked
  for (i = 0; i < max_clients; i++) {
    client_socket[i] = -1;
  }

  //create a master socket
  if ((master_socket = socket(AF_INET, SOCK_STREAM, 0)) == 0) {
    logger(LOG_ERROR_LEVEL, "socket failed");
    exit(EXIT_FAILURE);
  }

  //set master socket to allow multiple connections ,
  //this is just a good habit, it will work without this
  if (setsockopt(master_socket, SOL_SOCKET, SO_REUSEADDR, (char *) &opt,
                 sizeof(opt)) < 0) {
    logger(LOG_ERROR_LEVEL, "setsockopt");
    exit(EXIT_FAILURE);
  }

  //type of socket created
  address.sin_family = AF_INET;
  address.sin_addr.s_addr = INADDR_ANY;
  address.sin_port = htons(PORT);

  //bind the socket to localhost port 8888
  if (bind(master_socket, (struct sockaddr *) &address, sizeof(address)) < 0) {
    logger(LOG_ERROR_LEVEL, "bind failed");
    exit(EXIT_FAILURE);
  }
  logger(LOG_INFO_LEVEL, "Listener on port %d \n", PORT);

  //try to specify maximum of 3 pending connections for the master socket
  if (listen(master_socket, 3) < 0) {
    logger(LOG_ERROR_LEVEL, "listen");
    exit(EXIT_FAILURE);
  }

  //accept the incoming connection
  addrlen = sizeof(address);
  logger(LOG_INFO_LEVEL, "Waiting for connections ...\n");

  while (TRUE) {
    time_t seconds = time(NULL);
    timeinfo = localtime(&seconds);
    if (update_will_be_needed(timeinfo))
      set_will_be(users);
    if (((timeinfo->tm_hour == start_hour && timeinfo->tm_min >= start_min) || (timeinfo->tm_hour > start_hour)) &&
        current_question != 5)
      in_broadcast_time = 1;
    check_ads(users);

    if (in_broadcast_time)
      broadcast_question(questions, users);
    //clear the socket set
    FD_ZERO(&readfds);
    //add master socket to set
    FD_SET(0, &readfds);
    FD_SET(master_socket, &readfds);
    max_sd = master_socket;

    //add child sockets to set
    for (i = 0; i < max_clients; i++) {
      //socket descriptor
      sd = client_socket[i];

      //if valid socket descriptor then add to read list
      if (sd > 0)
        FD_SET(sd, &readfds);

      //highest file descriptor number, need it for the select function
      if (sd > max_sd)
        max_sd = sd;
    }

    //wait for an activity on one of the sockets , timeout is NULL ,
    //so wait indefinitely
    activity = select(max_sd + 1, &readfds, NULL, NULL, &tv);

    if ((activity < 0) && (errno != EINTR)) {
      logger(LOG_ERROR_LEVEL, "select error");
    }

    //If something happened on the master socket ,
    //then its an incoming connection
    if (FD_ISSET(master_socket, &readfds)) {
      if ((new_socket = accept(master_socket,
                               (struct sockaddr *) &address, (socklen_t * ) & addrlen)) < 0) {
        logger(LOG_ERROR_LEVEL, "accept");
        exit(EXIT_FAILURE);
      }

      //inform user of socket number - used in send and receive commands
      logger(LOG_INFO_LEVEL, "New connection , socket fd is %d , ip is : %s , port : %d\n", new_socket,
             inet_ntoa(address.sin_addr),
             ntohs(address.sin_port));
      //send new connection greeting message
      if (send(new_socket, message, strlen(message), 0) != strlen(message)) {
        logger(LOG_ERROR_LEVEL, "send");
      }

      logger(LOG_INFO_LEVEL, "Welcome message sent successfully\n");

      //add new socket to array of sockets
      for (i = 0; i < max_clients; i++) {
        //if position is empty
        if (client_socket[i] == -1) {
          client_socket[i] = new_socket;
          logger(LOG_INFO_LEVEL, "Adding to list of sockets as %d\n", i);
          break;
        }
      }
    }
    if (FD_ISSET(0, &readfds)) {
      if ((valread = read(0, buffer, 1024)) != 0) {
        buffer[valread] = '\0';
        for (i = 0; i < max_clients; i++) {
          if (client_socket[i] != -1) {
            send(client_socket[i], buffer, strlen(buffer), 0);
          }
        }
      }
    }

    //else its some IO operation on some other socket
    for (i = 0; i < max_clients; i++) {
      int jj;
      sd = client_socket[i];

      if (FD_ISSET(sd, &readfds)) {
        //Check if it was for closing , and also read the
        //incoming message
        if ((valread = read(sd, buffer, 1024)) == 0) {
          //Somebody disconnected , get his details and print
          getpeername(sd, (struct sockaddr *) &address, \
            (socklen_t * ) & addrlen);
          logger(LOG_INFO_LEVEL, "Host disconnected , ip %s , port %d \n",
                 inet_ntoa(address.sin_addr), ntohs(address.sin_port));

          //Close the socket and mark as 0 in list for reuse
          close(sd);
          client_socket[i] = -1;
          jj = get_user_by_sd(sd, users);
          if (jj >= 0)
            users[jj].sd = -1;
        } else {
          buffer[valread - 1] = '\0';
          logger(LOG_INFO_LEVEL, "from client %d with port %d received %s\n", i, ntohs(address.sin_port), buffer);
          process_request(sd, buffer, users, questions);
        }
      }
    }
  }

  return 0;
}
