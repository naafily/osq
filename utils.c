//
// Created by partizaans on 3/1/19.
//
#include "utils.h"


void sysprint(char *format, ...) {
  va_list arguments;
  va_start(arguments, format);
  char screen_msg[1000] = {};
  vsprintf(screen_msg, format, arguments);
  write(STDOUT_FILENO, screen_msg, sizeof(screen_msg) - 1);
  va_end(arguments);
}

void colorprint(char *format, char *color, ...) {
  va_list arguments;
  va_start(arguments, format);
  char new_format[1000];
  char screen_msg[1000] = {};
  sprintf(new_format, "%s%s%s", color, format, KNRM);
  vsprintf(screen_msg, new_format, arguments);
  write(STDOUT_FILENO, screen_msg, sizeof(screen_msg) - 1);
  va_end(arguments);

}

void logger(char *tag, char *format, ...) {
  char *color = (strncmp(tag, LOG_ERROR_LEVEL, strlen(LOG_ERROR_LEVEL)) == 0) ? KRED : KYEL;
  time_t now;
  time(&now);
  va_list arguments;
  va_start(arguments, format);
  char message[500] = {};
  char log[500] = {};
  vsprintf(message, format, arguments);
  sprintf(log, "%s%s [%s]: %s%s\n", color, ctime(&now), tag, message, KNRM);
  write(STDOUT_FILENO, log, sizeof(log) - 1);
  va_end(arguments);
}