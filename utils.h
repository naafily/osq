//
// Created by partizaans on 3/1/19.
//

#ifndef OS_P1_UTILS_H
#define OS_P1_UTILS_H

#define STDOUT_FILENO 1
#define KNRM  "\x1B[0m"
#define KRED  "\x1B[31m"
#define KGRN  "\x1B[32m"
#define KYEL  "\x1B[33m"
#define KBLU  "\x1B[34m"
#define KMAG  "\x1B[35m"
#define KCYN  "\x1B[36m"
#define KWHT  "\x1B[37m"

#define LOG_INFO_LEVEL "INFO"
#define LOG_DEBUG_LEVEL "DEBUG"
#define LOG_ERROR_LEVEL "ERROR"
#define LOG_WARNING_LEVEL "WARNING"

#include <stdarg.h>
#include <stdio.h>
#include <time.h>

void sysprint(char *format, ...);

void colorprint(char *format, char *color, ...);

void logger(char *tag, char *message, ...);

#endif //OS_P1_UTILS_H
